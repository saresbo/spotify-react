import React from 'react';
import ReactDOM from 'react-dom';
import App from './Components/app.jsx';
import Login from './Components/login.jsx';
import List from './Components/List.jsx';
import User from './Components/User.jsx';
import PlayList from './Components/PlayList.jsx';
import ListDetail from './Components/ListDetail.jsx';
import Search from './Components/Search.jsx';
import ConectionServices from './js/ConectionServices.js';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

var user = {};

ReactDOM.render(
    <Router history = {browserHistory}>
      <Route path = "/" component = {App}>
         <IndexRoute component = {Login} />
         <Route path = "/login" component = {Login} />
         <Route path = "/user/:accessToken" component = {User} />
         <Route path = "/playlist/:accessToken/:user" component = {PlayList} />
         <Route path = "/listdetail/:list" component = {ListDetail} />
         <Route path = "/search/:accessToken" component = {Search} />
      </Route>
   </Router>, document.getElementById('app'));
