import Spotify from 'spotify-web-api-js';
var spotifyApi = new Spotify();
import React from 'react';
import ReactDOM from 'react-dom';
import ConectionServices from '../js/ConectionServices.js';
import List from './List.jsx';

import { Router, Route, IndexRoute, browserHistory } from 'react-router';

class Login extends React.Component{

	constructor(props){
		super(props);
	}
	authenticationSpotify(){
		var conector = new ConectionServices();
		conector.login(function (accessToken) {
			browserHistory.push('/User/' + accessToken);
        });
	}

	render(){
		var style = {
			display: 'block',
			width: '100%',
			height: '100%'
		};
		var styleImgLogo = {
			float: 'left',
			width: '82%',
			maxWidth: 300
		};
		return(
			<article style={style}>
				<div className='formLogin'>
					<div className='itemForm'>
						<img style={styleImgLogo} src='img/logo.png' />
					</div>
					<div className='itemForm'>
						<button className='btnLogin' onClick={this.authenticationSpotify.bind(this)}>LOGIN</button>
					</div>
				</div>
			</article>
		);
	}
	
}

export default Login;