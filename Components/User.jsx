import Spotify from 'spotify-web-api-js';
var spotifyApi = new Spotify();
import React from 'react';
import ReactDOM from 'react-dom';
import ConectionServices from '../js/ConectionServices.js';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import Header from './Header.jsx';

var user = {};

class User extends React.Component{

	constructor(props){
		super(props);
		this.state = {userComplete: false};
	}

	componentWillMount(){
		const {dispatch, params} = this.props;
	    const {accessToken, refreshToken} = params;
	    this.getUserData(accessToken);
	}

	render(){
		var styleLoading = {
			width: '100%',
			height: (document.documentElement).clientHeight,
			background: 'rgba(0,0,0,0.6)'
		};
		var styleUserInfo = {
			width: '70%',
			margin: '0 auto'
		};
		var styleImg = {
			width: 110,
			position: 'absolute',
			top: '50%',
			left: '50%',
			marginTop: -55,
			marginLeft: -55
		};
		if(!this.state.userComplete){
			return(
				<div style={styleLoading}>
					<img style={styleImg} src='/img/loading.gif'/>
				</div>
			)
		}else{
			return(
				<div>
				<Header token = {this.props.params.accessToken} />
				<div style={styleUserInfo}>
					<div className='itemUser'>
						<h3>Datos del usuario</h3>
						<div className='photoUser'>
							<img style={{width:'100%'}} src={user.images[0].url} />
						</div>
						<p>User name: {user.display_name}</p>
						<p>E-mail: {user.email}</p>
						<p>Followers: {user.followers.total}</p>
						<p>URL: {user.external_urls.spotify}</p>
						<div className='btnPlayList'>
							<button className='btnLogin' onClick={this.getPlayLists.bind(this)}>Ver Listas</button>
						</div>
					</div>
				</div>
				</div>
			);
		}
	}

	getUserData(accessToken) {
		spotifyApi.setAccessToken(accessToken);
		spotifyApi.getMe().then(data => {
		if(data.images.length <= 0){
			data.images.push({url:'https://www.rit.edu/science/sites/rit.edu.science/files/root/man-placeholder_27.jpg'});
		}
		  user = Object.assign({}, data);
		  this.setState({userComplete: true});
		  console.log("USER = "+user);
		  //this.getUserPlayList(user.id);

		}).catch(e => {
		  console.log('e', e);
		});
	}

	getPlayLists(){
		browserHistory.push('/playlist/' + this.props.params.accessToken + '/' + user.id);
	}
	
}

export default User;