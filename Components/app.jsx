import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import Login from './login.jsx';
import List from './List.jsx';


class App extends React.Component{

	render(){
		return(
	    	<div>
				{this.props.children}
	        </div>
		);
	}

}

export default App;