import Spotify from 'spotify-web-api-js';
var spotifyApi = new Spotify();
import React from 'react';
import ReactDOM from 'react-dom';
import ItemPlayList from './ItemPlayList.jsx';

class Track extends React.Component{
	constructor(props){
		super(props);
	}
	render(){
		return(
			<div className='itemUser'>
				<div>
					<div className='trackItem'>
						<div className='textTrack'>
							<span>{this.props.track.artists[0].name} - </span>
							<span>{this.props.track.name}</span>
							
							<audio controls>
							  	<source src={this.props.track.preview_url} type="audio/mpeg" />
								Your browser does not support the audio element.
							</audio>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Track;