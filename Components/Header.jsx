import Spotify from 'spotify-web-api-js';
var spotifyApi = new Spotify();
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import User from './User.jsx';
import Search from './Search.jsx';

class Header extends React.Component{
	constructor(props){
		super(props);
	}
	render(){
		return(
            <div className='header'>
                <ul>
                    <li><a href='javascript:void(0)' onClick={this.showProfile.bind(this)} >Perfil</a></li>
                    <li><a href='javascript:void(0)' onClick={this.showSearch.bind(this)}>Buscar canciones</a></li>
					<li><a href='javascript:void(0)' onClick={this.logout.bind(this)}>Cerrar sesión</a></li>
                </ul>
            </div>
		);
	}
    showSearch(){
		browserHistory.push('/search/' + this.props.token);
	}
    showProfile(){
		browserHistory.push('/user/' + this.props.token);
	}
	logout(){
		browserHistory.push('/login');
	}
}

export default Header;