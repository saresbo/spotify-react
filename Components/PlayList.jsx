import Spotify from 'spotify-web-api-js';
var spotifyApi = new Spotify();
import React from 'react';
import ReactDOM from 'react-dom';
import ItemPlayList from './ItemPlayList.jsx';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

class PlayList extends React.Component{

	constructor(props){
		super(props);
		this.state = {playListsComplete: false, playList: ""};
	}

	componentWillMount(){
		const {dispatch, params} = this.props;
	    const {accessToken, user, refreshToken} = params;
	    this.getUserPlayList(accessToken);
	}
	
	getUserPlayList(accessToken) {
		spotifyApi.setAccessToken(accessToken);
		spotifyApi.getUserPlaylists()
		  .then(data => {
		  	this.setState({playListsComplete: true, playList: data.items});
		  }).catch(e => {
		  	console.log('e', e);
		  });
	}

	goBack(){
		browserHistory.push('/user/' + this.props.params.accessToken);
	}

	render(){
		var styleLoading = {
			width: '100%',
			height: (document.documentElement).clientHeight,
			background: 'rgba(0,0,0,0.6)'
		};
		var styleUserInfo = {
			width: '70%',
			margin: '0 auto'
		};
		var styleImg = {
			width: 110,
			position: 'absolute',
			top: '50%',
			left: '50%',
			marginTop: -55,
			marginLeft: -55
		};

		if(!this.state.playListsComplete){
			return(
				<div style={styleLoading}>
					<img style={styleImg} src='/img/loading.gif'/>
				</div>
			)
		}else{
			return(
				<div style={styleUserInfo}>
					<div className='itemUser'>
						<h3>PLAYLISTS</h3>
						{
							(this.state.playList).map(list =>
								<ItemPlayList key={list.id} itemList = {list} token = {this.props.params.accessToken} user={this.props.params.user} />
							)
						}
					</div>
					<div className='btnPlayList'>
						<button className='btnLogin' onClick={this.goBack.bind(this)}>Volver</button>
					</div>
				</div>
			);
		}

	}
	
}

export default PlayList;