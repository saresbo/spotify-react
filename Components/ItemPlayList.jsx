import Spotify from 'spotify-web-api-js';
var spotifyApi = new Spotify();
import React from 'react';
import ReactDOM from 'react-dom';
import PlayList from './PlayList.jsx';
import User from './User.jsx';
import Track from './Track.jsx';

class ItemPlayList extends React.Component{
	constructor(props){
		super(props);
		this.state = {loadTracks: false, tracks: ""};
	}
	render(){
		var imgSize = {
			width: 300
		};
		if(!this.state.loadTracks){
			return(
				<div className='itemUser'>
					<a href='javascript:void(0)' onClick={this.getTracks.bind(this)}>
					<div>
						<p>PLAYLIST: {this.props.itemList.name}</p>
						<div>
							<div className='photoUser'>
								<img style={imgSize} src={this.props.itemList.images[0].url} />
							</div>
						</div>
					</div>
					</a>
					<div>
					</div>
				</div>
			);
		}else{
			return(
				<div className='itemUser'>
					<a href='javascript:void(0)' onClick={this.getTracks.bind(this)}>
					<div>
						<p>PLAYLIST: {this.props.itemList.name}</p>
						<div>
							<div className='photoUser'>
								<img style={imgSize} src={this.props.itemList.images[0].url} />
							</div>
						</div>
					</div>
					</a>
					<div>
					{
						this.state.tracks.map(track =>
							<Track track={track.track} key={track.track.id} />
						)
					}
					</div>
				</div>
			);
		}
	}
	getTracks(){
		//browserHistory.push('/listdetail/' + this.props.itemList.id);
		spotifyApi.setAccessToken(this.props.token);
		spotifyApi.getPlaylist(this.props.user, this.props.itemList.id)
		  .then(data => {
		    console.log('User playlist', data);
		    this.setState({loadTracks: true, tracks: data.tracks.items});
		  }).catch(e => {
		    console.error(e);
		  });
	}
}

export default ItemPlayList;