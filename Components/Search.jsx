import Spotify from 'spotify-web-api-js';
var spotifyApi = new Spotify();
import React from 'react';
import ReactDOM from 'react-dom';
import Track from './Track.jsx';
import Header from './Header.jsx';

class Search extends React.Component{

	constructor(props){
		super(props);
        this.state = {resultsData: '', resultsFound: false};
	}

	componentWillMount(){
		const {dispatch, params} = this.props;
	    const {accessToken, user, refreshToken} = params;
	}
	
	getSearchResult() {
        let text = document.getElementById('inputSearch').value;
        spotifyApi.searchTracks(text)
        .then(data => {
            this.setState({resultsData: data.tracks.items, resultsFound: true});
            console.log('Search by "Love" '+this.state.resultsData);
        }).catch(e => {
            console.error(e);
        });
	}

	render(){
		var styleLoading = {
			width: '100%',
			height: (document.documentElement).clientHeight,
			background: 'rgba(0,0,0,0.6)'
		};
		var styleUserInfo = {
			width: '70%',
			margin: '0 auto'
		};
		var styleImg = {
			width: 110,
			position: 'absolute',
			top: '50%',
			left: '50%',
			marginTop: -55,
			marginLeft: -55
		};

        if(this.state.resultsFound){
            return(
                <div>
                    <Header token = {this.props.params.accessToken} />
                    <div style={styleUserInfo}>
                        <div>
                            <input id='inputSearch' type='text' />
                            <a className='btnSearch' href='javascript:void(0)' onClick={this.getSearchResult.bind(this)}>Buscar</a>
                        </div>
                        {
                            this.state.resultsData.map(item =>
                                <Track track={item} key={item.id} />
                            )
                        }
                    </div>
                </div>
            );

        }else{
            return(
                <div>
                    <Header token = {this.props.params.accessToken} />
                    <div style={styleUserInfo}>
                        <div>
                            <input id='inputSearch' type='text' />
                            <a className='btnSearch' href='javascript:void(0)' onClick={this.getSearchResult.bind(this)}>Buscar</a>
                        </div>
                    </div>
                </div>
            );
        }

	}
	
}

export default Search;