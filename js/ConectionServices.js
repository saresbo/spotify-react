import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import User from '../Components/User.jsx';

class ConectionServices{

    login(callback) {
        var REDIRECT_URI = 'http://localhost:7777/callback/';
        var CLIENT_ID = 'a3c897b5b59d4da693b2adbb10c764a4';
        var CLIENT_SECRET = 'e0ab75c5a7e34728a445c27c5c4f9a34';
        
        function getLoginURL(scopes) {
            return 'https://accounts.spotify.com/authorize?client_id=' + CLIENT_ID +
                '&redirect_uri=' + encodeURIComponent(REDIRECT_URI) +
                '&scope=' + encodeURIComponent(scopes.join(' ')) +
                '&response_type=token';
        }

        var url = getLoginURL([
            'user-read-email'
        ]);

        var width = 450,
            height = 630,
            left = (screen.width / 2) - (width / 2),
            top = (screen.height / 2) - (height / 2);

        window.addEventListener("message", function (event) {
            var hash = JSON.parse(event.data);
            if (hash.type == 'access_token') {
                callback(hash.access_token);
            }
        }, false);

        var w = window.open(url,
            'Spotify',
            'menubar=no,location=no,resizable=no,scrollbars=no,status=no, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left
        );
    }
    getUserData(accessToken) {
      var CLIENT_ID = 'a3c897b5b59d4da693b2adbb10c764a4';
      var CLIENT_SECRET = 'e0ab75c5a7e34728a445c27c5c4f9a34';
      var url = 'https://api.spotify.com/v1/me';

      var httpRequest = new XMLHttpRequest();

      httpRequest.onreadystatechange = function(){
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
          if (httpRequest.status === 200) {
            user = JSON.parse(httpRequest.responseText);
            browserHistory.push('/User/' + accessToken);
          } else {
            alert('There was a problem with the request.');
          }
        }
      };

      httpRequest.open('GET', url);
      httpRequest.setRequestHeader('Authorization', 'Bearer ' + accessToken);
      httpRequest.send(); 

      /*return $.ajax({
          url: 'https://api.spotify.com/v1/me',
          headers: {
             'Authorization': 'Bearer ' + accessToken
          }
      });*/
    }
    
}

export default ConectionServices;
